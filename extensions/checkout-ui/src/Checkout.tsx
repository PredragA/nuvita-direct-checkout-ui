import {
    Text,
    InlineLayout,
    useTranslate,
    reactExtension,
    useAttributeValues
} from '@shopify/ui-extensions-react/checkout';

export default reactExtension(
    'purchase.checkout.block.render',
    () => <Extension/>,
);

function Extension() {
    const translate = useTranslate();
    let [sponsorInfo]: string[] = useAttributeValues(['sponsorInfo']);

    let sponsorInfoParsed: { "firstName": string, "lastName": string } = JSON.parse(sponsorInfo);
    let fullName: string = "\t" + sponsorInfoParsed?.firstName.toUpperCase() + " " + sponsorInfoParsed?.lastName.toUpperCase();
    return (
        <InlineLayout columns={['fill', 'auto', 5, 'auto']}>
            <Text></Text>
            <Text>{translate('shoppingWithUpperCase')}</Text>
            <Text></Text>
            <Text emphasis="bold" appearance="accent"> {fullName}</Text>
        </InlineLayout>
    );
}